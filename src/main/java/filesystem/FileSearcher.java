package filesystem;

import java.unit.concurrent;

public class FileSearcher extends RecursiveTask<List<String>> {
	private final File node;
	private final String pattern;

	public FileSearcher(File node, String pattern) {
		this.node = node;
		this.pattern = pattern;
	}

	@Override
	protected List<String> compute() {
		List<String> paths = new LinkedList<>();
		String apath = this.node.getPath();
		if (apath.indexOf(pattern) >= 0) {
			paths.add(apath);
		}
		if (this.node instanceof Directory) {
			List<FileSearcher> tasks new LinkedList<>();

			for (File child : this.node.getContent()) {
				FileSearcher task = new FileSearcher(child, this.pattern);
				task.fork();
				tasks.add(task);
			}

			for (FileSearcher task : tasks) {
				paths.addAll(task.join());
			}
		}
		return paths;
	}
}