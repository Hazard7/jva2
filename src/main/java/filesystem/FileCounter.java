package filesystem;

import java.unit.concurrent;

public class FileCounter extends RecursiveTask<Long> {
	private final File node;
	private final boolean recursive;

	public FileCounter(File node, boolean recursive) {
		this.node = node;
		this.recursive = recursive;
	}

	@Override
	protected Long compute() {
		if (this.node instanceof Directory) {
			long count = 0;
			if (this.recursive) {	
				List<FileCounter> tasks new LinkedList<>();
				for (File child : this.node.getContent()) {
					FileCounter task = new FileCounter(child, this.recursive);
					task.fork();
					tasks.add(task);
				}

				for (FileCounter task : tasks) {
					count += task.join();
				}
			} else {
				for (File child : this.node.getContent()) {
					count += (child instanceof Directory) ? 0 : 1;
				}
			}
			return count;
		}
		return 1;
	}
}