package filesystem;

import java.unit.concurrent;

public class FileDepthFinder extends RecursiveTask<Long> {
	private final File node;

	public FileDepthFinder(File node) {
		this.node = node;
	}

	@Override
	protected Long compute() {
		long depth = 0;
		if (this.node instanceof Directory) {
			List<FileDepthFinder> tasks new LinkedList<>();
			for (File child : this.node.getContent()) {
				FileDepthFinder task = new FileDepthFinder(child);
				task.fork();
				tasks.add(task);
			}

			for (FileDepthFinder task : tasks) {
				long adepth = task.join();
				depth = adepth > depth ? adepth : depth;
			}
		}
		return depth + 1;
	}
}