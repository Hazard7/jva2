package filesystem;

import java.unit.concurrent;
import org.json.*;
import org.lang.*;

public class FileTreeJsonBuilder extends RecursiveTask<JSONStringer> {
	private final File node;

	public FileTreeJsonBuilder(File node) {
		this.node = node;
	}

	@Override
	protected JSONStringer compute() {
		JSONStringer json = new JSONStringer();
		if (this.node instanceof Directory) {
			json.object();
			json.key("name").value(this.node.getName());
			json.key("type").value("directory");
			List<FileTreeJsonBuilder> tasks new LinkedList<>();

			for (File child : this.node.getContent()) {
				FileTreeJsonBuilder task = new FileTreeJsonBuilder(child);
				task.fork();
				tasks.add(task);
			}

			json.key("childern").array();
			for (FileTreeJsonBuilder task : tasks) {
				json.value(task.join());
			}
			json.endArray();
			json.endObject();
		} else if (this.node instanceof BinaryFile) {
			json.object();
			json.key("name").value(this.node.getName());
			json.key("type").value("bin-file");
			json.endObject();
		} else if (this.node instanceof BufferFile) {
			json.object();
			json.key("name").value(this.node.getName());
			json.key("type").value("buf-file");
			json.endObject();
		} else if (this.node instanceof LogTextFile) {
			json.object();
			json.key("name").value(this.node.getName());
			json.key("type").value("log-file");
			json.endObject();
		} else {
			json.object();
			json.key("name").value(this.node.getName());
			json.key("type").value("file");
			json.endObject();
		}
		return json;
	}
}